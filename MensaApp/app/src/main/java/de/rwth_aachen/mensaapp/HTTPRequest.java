package de.rwth_aachen.mensaapp;

import android.os.AsyncTask;

import java.io.*;
import java.net.*;
import java.util.Arrays;
import java.util.HashMap;
import java.util.zip.GZIPInputStream;

/**
 * Class which implements the HTTPRequest
 * Created by admin on 25.04.2015.
 */
public class HTTPRequest extends AsyncTask<String,Integer,String> {

    public static int minmumHeaderLength = 18;

    @Override
    protected String doInBackground(String... hostAndPage){
        // Variables
        Socket sock;
        PrintWriter pw;
        BufferedReader rd = null;
        StringBuilder sb;
        String line;
        String result = "";
        String headerString="";
        InetAddress inetAddress;
        byte input[]=null;
        byte header[]=null;
        byte content[]=null;
        HTTPHeader header_type = new HTTPHeader();
        HashMap<String,String> header_map = new HashMap<String,String>();

        try {
            inetAddress = InetAddress.getByName(hostAndPage[0]);  // Get address
            sock = new Socket(inetAddress,80);  // Open socket
            pw = new PrintWriter(sock.getOutputStream());   // Send HTTP request via PrintWriter in socket
            pw.println("GET "+hostAndPage[1]+" HTTP/1.1");
            pw.println("Host: "+hostAndPage[0]);
            pw.println("User-Agent: "+ System.getProperty("http.agent"));
            pw.println("Accept: text/html");
            pw.println("Accept-Encoding: gzip");
            pw.println("Connection: close");
            pw.println("Cache-Control: no-cache");
            pw.println("");
            pw.flush();

            // Get byte stream
            InputStream in = sock.getInputStream();
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            byte buffer[] = new byte[1024];
            int n;
            while( (n=in.read(buffer)) != -1 ){
                baos.write(buffer, 0, n);
            }
            input = baos.toByteArray();

            if(input.length<minmumHeaderLength){ //Header to small
                return "";
            }

            // Split header and content
            for(int i=0; i<input.length-4; i++){
                if(input[i]==0x0d && input[i+1]==0x0a && input[i+2]==0x0d && input[i+3]==0x0a){
                    header = Arrays.copyOfRange(input,0,i+3);
                    content = Arrays.copyOfRange(input,i+4,input.length);
                    break;
                }
            }

            // Handle header
            if(header==null){
                return "";  // just return "" to handle error later
            }

            sb = new StringBuilder();
            rd = new BufferedReader(new InputStreamReader(new ByteArrayInputStream(header), "UTF-8"));  // Convert header bytes to String
            while (((line = rd.readLine()) != null)) {
                sb.append(line + "\n");
            }
            headerString = sb.toString();

            header_map = header_type.parseHeader(headerString);

            if(!header_map.containsValue("200 OK")){ //Response not valid
                return "";
            }

            if(header_map.containsValue("chunked")) {
                // Handle content
                for (int i = 0; i < content.length - 4; i++) {
                    if (content[i] == 0x0d && content[i + 1] == 0x0a) {
                        content = Arrays.copyOfRange(content, i + 2, content.length);   // Cut away chunk boundary (start)
                        break;
                    }
                }
                content = Arrays.copyOfRange(content, 0, content.length - 7);   // Cut away chunk boundary (end)
            }

            if(header_map.containsValue("gzip")) {
                rd = new BufferedReader(new InputStreamReader(new GZIPInputStream(new ByteArrayInputStream(content)), "UTF-8"));    // Decompress content
                sb = new StringBuilder();
                while (((line = rd.readLine()) != null)) {
                    sb.append(line + "\n");
                }
                result = sb.toString(); // Store decompressed content in result
            }else{
                rd = new BufferedReader(new InputStreamReader((new ByteArrayInputStream(content)), "UTF-8"));
                sb = new StringBuilder();
                while (((line = rd.readLine()) != null)) {
                    sb.append(line + "\n");
                }
                result = sb.toString();
            }
        } catch (UnknownHostException e) {
            e.printStackTrace();
        } catch (IOException e) {
            return "";  // just return "" to handle error later
        } finally {
            if (rd != null) {
                try {
                    rd.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return result;
    }
}
