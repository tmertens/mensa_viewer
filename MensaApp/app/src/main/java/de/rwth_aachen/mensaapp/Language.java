package de.rwth_aachen.mensaapp;

/**
 * Enum class form supported languages.
 * Created by admin on 28.04.2015.
 */
public enum Language {
    GER,ENG
}
