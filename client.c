// Includes
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <unistd.h>
#include <sys/time.h>

// Definitions
#define BUFFER_SIZE 1024*8
#define MIN_HEADER_BYTES 18
#define USER_AGENT "TODO"

/*
 * Send all data
 */
int send_all(int sockfd, void *buffer, size_t length, int flags){

    int num=0;
    int sendBytes=0;
    size_t remainingLength = length;                // remaining amount to send
    char *currentBufferPtr = (char *) buffer;       // points to current free position in buffer

    while(remainingLength>0){

        num=send(sockfd, currentBufferPtr, remainingLength, flags);       // call default send
        sendBytes +=num;                // incease counter of send bytes
        if(num==0){
            return sendBytes;           // all data has been send
        }else if(num==-1){
            return -1;                  // error
        }

        remainingLength -= num;         // reduce remaining amount to send
        currentBufferPtr += num;        // shift buffer position to next data
    }
    return sendBytes;
}

/*
 * Receive all data
 */
int recv_all(int sockfd, void *buffer, size_t bufferLength, int flags){

    int num=0;
    int receivedBytes=0;
    size_t remainingBufferLength = bufferLength;    // remaining amount to read
    char *currentBufferPtr = (char *) buffer;       // points to current free position in buffer

    while(remainingBufferLength>0){

        num=recv(sockfd, currentBufferPtr, remainingBufferLength, flags);       // call default recv
        receivedBytes +=num;            // incease counter of received bytes
        if(num==0){
            return receivedBytes;       // all data has been received
        }else if(num==-1){
            return -1;                  // error or timeout
        }

        remainingBufferLength -= num;   // reduce remaining amount to read
        currentBufferPtr += num;        // shift buffer position to not overwrite existing data
    }
    return receivedBytes;
}

/*
 *  Returns content (HTML) of the HTTP Request
 *  @return HTML content as String or NULL on error
 */
char* getHTML(char *servername, char *port, char *page){

	// Variables
	int num,sockfd, error;
    struct addrinfo hints;
	struct addrinfo *serverinfo;

    char buffer[BUFFER_SIZE+1]; // +1 so that we can always add a null termination
    char *html=malloc(BUFFER_SIZE+1);   // equal to buffer
    char request[1000] = {0};   // max size 1000

	// Get server address
	memset(&hints, 0, sizeof(hints));   // init all to 0
	hints.ai_family = AF_UNSPEC;        // accept IPv4 and IPv6
	hints.ai_socktype = SOCK_STREAM;    // TCP like connection
	hints.ai_flags = AI_PASSIVE;

	error = getaddrinfo(servername, port, &hints, &serverinfo);
	if(error < 0){
		return NULL;
	}

	// Open socket
	sockfd = socket(serverinfo->ai_family, serverinfo->ai_socktype, IPPROTO_TCP);
	if(sockfd < 0){
		return NULL;
	}

	// Data transmission
    error = connect(sockfd, serverinfo->ai_addr, serverinfo->ai_addrlen);
    if(error < 0){
        return NULL;
    }

    // Create GET request
    sprintf(request, "GET %s HTTP/1.1\r\n"
		"Host %s\r\n"
		"User-Agent"
		"Accept: text/html\r\n\r\n"
		,page,servername,USER_AGENT);

    // Send GET request
    error = send_all(sockfd,request,sizeof(request),0);
    if(error<=0){
        return NULL;
    }

    // Introduce variables for select
    fd_set rfds;
    struct timeval tv;
    int retval;

    // Watch the socketfd to see when it has input
    FD_ZERO(&rfds);
    FD_SET(sockfd,&rfds);

    // Set timeout to 15 seconds
    tv.tv_sec = 15;
    tv.tv_usec = 0;

    // Use select with timeout for sockfd
    retval = select(sockfd+1, &rfds, NULL, NULL, &tv);
    if(retval == -1){
        return NULL;
    }else if(FD_ISSET(sockfd,&rfds)){
    }else{
        return NULL;
    }

    // Receive response from server
	if ( (num = recv_all(sockfd, buffer, BUFFER_SIZE,0)) <=0) {
        return NULL;
    }
	buffer[num] = '\0'; // add null termination for buffer

	// Check if received enough bytes for header parsing
	if(num < MIN_HEADER_BYTES){
        return NULL;
	}

    // Separate header
    char delimiter[]="\r\n";
    char *ptr;

    ptr=strtok(buffer, delimiter);

    // HTTP/1.1 200 OK
    if(strncmp(ptr,"HTTP/1.1 200 OK",15)==-1){
        return NULL;
    }
    ptr=strtok(NULL, delimiter);    // Next header line

    // Server: nginx
    if(strncmp(ptr,"Server:",7)==-1){  // Compare String
        return NULL;
    }
    ptr=strtok(NULL, delimiter);    // Next header line

    // Date: ...
    if(strncmp(ptr,"Date:",5)==-1){  // Compare String
        return NULL;
    }
    ptr=strtok(NULL, delimiter);    // Next header line

    // Content-Type: text/html
    if(strncmp(ptr,"Content-Type: text/html",23)==-1){  // Compare String
        return NULL;
    }
    ptr=strtok(NULL, delimiter);    // Next header line

    // Content-Length: ...
    int contentLength = -1;
    sscanf(ptr,"%*[^0-9]%d",&contentLength);    // Get content length
    if(contentLength<=0){
        return NULL;
    }
    ptr=strtok(NULL, delimiter);    // Next header line

    // Connection: close
    if(strncmp(ptr,"Connection: close",23)==-1){    // Compare String
        return NULL;
    }
    ptr=strtok(NULL, delimiter);    // Next header line

    // Get HTML data
    int currentLength=0;
    int iterations=0;
    while(ptr!=NULL){
        strncpy(&html[currentLength], ptr,BUFFER_SIZE);
        currentLength += strlen(ptr);
        ptr=strtok(NULL, delimiter);    // Next header line
        iterations++;
    }
    currentLength+=iterations;

    // Check length of received content
    if(contentLength != currentLength){
        return NULL;
    }

    // Close socket
    if(shutdown(sockfd,2) != 0){    // first shutdown
		return NULL;
	}
    if(close(sockfd) != 0){        // afterwards close
		return NULL;
	}

	return html;
}

/*
 *  Main function
 */
int main(){

    // For testing purpose
    char *result = getHTML("www.studentenwerk-aachen.de","80","/speiseplaene/vita-w.html");
	printf("%s\n", result);

}
