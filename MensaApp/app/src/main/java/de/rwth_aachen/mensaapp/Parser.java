package de.rwth_aachen.mensaapp;

import org.w3c.dom.*;
import org.xml.sax.InputSource;
import java.io.StringReader;
import javax.xml.parsers.*;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathFactory;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * Created by Thomas on 27.04.2015.
 */
public class Parser {

    /**
     *
     * @param text
     * @return
     */
    public String formatText(String text){
        String result = text;
        if(result.contains("200 OK")){
            int index  = result.indexOf("<?xml");
            result.substring(index,result.length());
        }
        result = result.replaceAll("  "," ");
        result = result.replaceAll("\n", "");
        result = result.replaceAll("\t","");
        result = result.replaceAll("<sup>","[");
        result = result.replaceAll("</sup>","]");

        switch(MainActivity.getCurrentLanguage()){
            case ENG: result = result.replaceAll("&","and"); result = result.replaceAll("<span class=\"or\">or</span>","or"); break;
            default: result = result.replaceAll("&","und"); result = result.replaceAll("<span class=\"or\">oder</span>","oder"); break;
        }

        return result;
    }

    /**
     *
     * @param text
     * @return
     */
    public List<MenuDay> parseXML(String text){

        List<MenuDay> result = new ArrayList<>();

        // Temporary variables
        String expression;
        NodeList tmpNodeList;
        Node tmpNode;
        String tmpString;
        String tmpStringArray[];
        String category;
        String name="";
        String price;
        String extra;
        String salad;
        String comment;
        String date;
        String day;

        try {
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            DocumentBuilder db = dbf.newDocumentBuilder();
            InputSource is = new InputSource();
            is.setCharacterStream(new StringReader(text));

            Document xmlDocument = db.parse(new ByteArrayInputStream(text.getBytes()));
            XPath xPath =  XPathFactory.newInstance().newXPath();

            // Get comment (once)
            expression = "/html/body/div/div/div/div/div[2]/div/p";
            tmpNodeList = (NodeList) xPath.compile(expression).evaluate(xmlDocument, XPathConstants.NODESET);
            comment = tmpNodeList.item(0).getFirstChild().getNodeValue();   // Get comment

            // Get days
            expression = "/html/body/div/div/div/div/div[1]/h3";
            NodeList nodeList = (NodeList) xPath.compile(expression).evaluate(xmlDocument, XPathConstants.NODESET);

            for (int dayIndex = 0; dayIndex < nodeList.getLength(); dayIndex++) {

                tmpNode = nodeList.item(dayIndex).getFirstChild();  // Goto h3[dayIndex+1]/a

                tmpString =  tmpNode.getFirstChild().getNodeValue();    // Split String
                switch(MainActivity.getCurrentLanguage()){
                    case ENG: tmpStringArray=tmpString.split(" ");
                        date = tmpStringArray[1] + " " + tmpStringArray[2] + " " + tmpStringArray[3];
                        day =  tmpStringArray[0];

                        break;   // date and name divided by space
                    default: tmpStringArray=tmpString.split(",");
                        date = tmpStringArray[1];
                        day =  tmpStringArray[0];
                        break;    // date and name divided by comma
                }

                // Create new MenuDay object
                MenuDay newDay = new MenuDay(dayIndex, date , day);   // tmpStringArray[1]=date, tmpStringArray[1]=name

                tmpNode = nodeList.item(dayIndex).getParentNode();  // Goto /html/body/div/div/div/div/div[1] (absolute path)
                NodeList nodeList2 = tmpNode.getChildNodes().item(2*dayIndex+1).getFirstChild().getFirstChild().getChildNodes(); // Goto /div[dayIndex+1]/table[1]/tbody/
                // 2*dayIndex+1, because there is an alternating sequence h3[1],div[1],h3[2],div[2],... of child nodes

                for (int menuIndex = 0; menuIndex < nodeList2.getLength(); menuIndex++) {
                    tmpNode = nodeList2.item(menuIndex).getFirstChild().getFirstChild();    // Goto tr[menuIndex+1]/td[1]

                    category = tmpNode.getFirstChild().getNodeValue();  // Get category
                    tmpNode = tmpNode.getNextSibling(); // Goto /td[2]
                    name = tmpNode.getFirstChild().getNodeValue();  // Get name
                    if((name.equals("geschlossen"))||name.equals("closed")) {
                        newDay.setSpecial(1);
                        switch(MainActivity.getCurrentLanguage()){
                            case ENG: newDay.setComment("Mensa closed. No menus available"); break;
                            default: newDay.setComment("Mensa geschlossen. Keine Menüs vorhanden"); break;
                        }
                    }
                    tmpNode = tmpNode.getNextSibling(); // Goto /td[3]
                    price = tmpNode.getFirstChild().getNodeValue(); // Get price

                    // Create MenuItem object and add to menuList of the current day
                    MenuItem newItem = new MenuItem(category,name,price);
                    newDay.listAddItem(newItem);
                }
                if(newDay.getSpecial()==0) {

                    tmpNode = nodeList.item(dayIndex).getParentNode();  // Goto /html/body/div/div/div/div/div[1] (absolute path)
                    tmpNodeList = tmpNode.getChildNodes().item(2*dayIndex+1).getLastChild().getFirstChild().getChildNodes(); // Goto /div[dayIndex+1]/table[2]/tbody/

                    tmpNode = tmpNodeList.item(0).getChildNodes().item(0).getLastChild().getFirstChild();  // Goto /tr[1]/td[2]
                    extra = tmpNode.getNodeValue();

                    tmpNode = tmpNodeList.item(1).getChildNodes().item(0).getLastChild().getFirstChild();  // Goto /tr[2]/td[2]
                    salad = tmpNode.getNodeValue();

                    newDay.setExtra(extra);
                    newDay.setSalad(salad);

                    newDay.setComment(comment); // Set comment
                }

                // Add day to result list
                result.add(newDay);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    return result;
    }
}
