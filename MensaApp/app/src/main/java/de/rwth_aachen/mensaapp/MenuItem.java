package de.rwth_aachen.mensaapp;

/**
 * Class to represent one Menu
 * Created by Thomas on 27.04.2015.
 */
public class MenuItem {
    private String category;
    private String name;
    private String price;

    /**
     *
     * @param category
     * @param name
     * @param price
     */
    public MenuItem(String category, String name, String price) {
        this.category = category;
        this.name = name;
        this.price = price;
    }

    /**
     *
     * @return category
     */
    public String getCategory() {
        return category;
    }

    /**
     *
     * @param category
     */
    public void setCategory(String category) {
        this.category = category;
    }

    /**
     *
     * @return name
     */
    public String getName() {
        return name;
    }

    /**
     *
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     *
     * @return price
     */
    public String getPrice() {
        return price;
    }

    /**
     *
     * @param price
     */
    public void setPrice(String price) {
        this.price = price;
    }

    @Override
    public String toString() {
        //return category + " : " + name + " (" + price + ")";
        return category + " (" + price + ") \n" +name;
    }
}
