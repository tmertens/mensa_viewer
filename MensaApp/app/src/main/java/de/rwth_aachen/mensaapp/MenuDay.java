package de.rwth_aachen.mensaapp;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Thomas on 27.04.2015.
 */
public class MenuDay {

    private int index;
    private String date;
    private String day;
    private String extra;
    private String salad;
    private int special;
    private String comment;

    private List<MenuItem> menu;

    /**
     *
     * @param index
     * @param date
     * @param day
     */
    public MenuDay(int index, String date, String day) {
        this.index = index;
        this.date = date;
        this.day = day;
        this.menu = new ArrayList<>();
        this.extra = "";
        this.salad = "";
        this.special = 0;
        this.comment = "";
    }

    /**
     *
     * @return Date
     */
    public String getDate() {
        return date;
    }

    /**
     *
     * @return Menu as List<MenuItem>
     */
    public List<MenuItem> getMenu() {
        return menu;
    }

    /**
     *
     * @param item
     */
    public void listAddItem (MenuItem item){
        menu.add(item);
    }

    /**
     *
     * @param item
     */
    public void listRemoveItem (MenuItem item){
        menu.remove(item);
    }

    /**
     *
     * @return index
     */
    public int getIndex() {
        return index;
    }

    /**
     *
     * @return extra
     */
    public String getExtra() {
        return extra;
    }

    /**
     *
     * @param extra
     */
    public void setExtra(String extra) {
        this.extra = extra;
    }

    /**
     *
     * @return salad
     */
    public String getSalad() {
        return salad;
    }

    /**
     *
     * @param salad
     */
    public void setSalad(String salad) {
        this.salad = salad;
    }

    /**
     *
     * @return special
     */
    public int getSpecial() {
        return special;
    }

    /**
     *
     * @param special
     */
    public void setSpecial(int special) {
        this.special = special;
    }

    /**
     *
     * @return comment
     */
    public String getComment() {
        return comment;
    }

    /**
     *
     * @param comment
     */
    public void setComment(String comment) {
        this.comment = comment;
    }

    @Override
    public String toString() {
        return day + ", " + date;
    }
}
