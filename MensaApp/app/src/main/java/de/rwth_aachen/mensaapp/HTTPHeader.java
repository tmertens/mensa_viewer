package de.rwth_aachen.mensaapp;

import java.util.HashMap;

/**
 * Is used to parse HTTPHeader into a HashMap
 */
public class HTTPHeader {

    /**
     * @param plainHeader
     * @return
     */
    public HashMap<String, String> parseHeader(String plainHeader){
        HashMap<String, String> header = new HashMap<String,String>();
        String[] parts = plainHeader.split("\n");
        String key = "";
        String value = "";
        for(int i=0; i < parts.length ; i++){
            int first_occ = parts[i].indexOf((":"));
            if(first_occ>0) {
                key = parts[i].substring(0, first_occ);
                value = parts[i].substring(first_occ+2, parts[i].length());
            }else{
                first_occ = parts[i].indexOf(" ");
                key = parts[i].substring(0, first_occ);
                value = parts[i].substring(first_occ+1, parts[i].length());
            }
            header.put(key,value);
        }
        return header;
    }
}
