package de.rwth_aachen.mensaapp;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBarDrawerToggle;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ExpandableListView;
import android.widget.ListView;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ExecutionException;

public class MainActivity extends AppCompatActivity {

    // Global variables
    private static Language currentLanguage=Language.GER;
    private static int currentMensa=0;

    private ListView mDrawerList;
    private ArrayAdapter<String> mAdapter;
    private DrawerLayout drawer;
    private ActionBarDrawerToggle mDrawerToggle;
    private DrawerLayout mDrawerLayout;
    private String currentTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        mDrawerList = (ListView)findViewById(R.id.navList);
        drawer = (DrawerLayout)findViewById(R.id.drawer_layout);

        String[] osArray = {"Mensa Ahorn", "Mensa Vita", "Mensa Academica", "Bistro Templergraben", "Mensa Bayernallee", "Mensa Eupenerstrasse", "Mensa Goethe"};
        mAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, osArray);
        mDrawerList.setAdapter(mAdapter);

        mDrawerLayout = (DrawerLayout)findViewById(R.id.drawer_layout);
        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout,
                R.string.drawer_open, R.string.drawer_close) {

            /** Called when a drawer has settled in a completely open state. */
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                setCurrentTitle(getTitle().toString());
                switch(MainActivity.getCurrentLanguage()){
                    case ENG: getSupportActionBar().setTitle("Select mensa..."); break;
                    default: getSupportActionBar().setTitle("Mensa auswählen..."); break;
                }

                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }

            /** Called when a drawer has settled in a completely closed state. */
            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
                //getSupportActionBar().setTitle(getCurrentTitle());
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }
        };

        mDrawerToggle.setDrawerIndicatorEnabled(true);
        mDrawerLayout.setDrawerListener(mDrawerToggle);



        // Add listener
        mDrawerList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                MainActivity.setCurrentMensa(position);
                loadMenu();
                drawer.closeDrawer(mDrawerList);

            }
        });

        // Add listener
        final ExpandableListView expListView = (ExpandableListView) findViewById(R.id.expandableListView);
        expListView.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {
            int previousGroup = -1;

            @Override
            public void onGroupExpand(int groupPosition) {
                if(groupPosition != previousGroup) {
                    expListView.collapseGroup(previousGroup);
                    previousGroup = groupPosition;
                }
            }
        });

        // Load a mensa at the beginning
        loadMenu();
    }

    /**
     *
     */
    public void loadMenu (){
        AsyncTask<String, Integer, String> http = new HTTPRequest();
        Parser parser = new Parser();
        String result = "";
        String host="";
        String page="";
        List<MenuDay> plan = null;

        HTTPRequest request = new HTTPRequest();

        // Get mensa
        host="www.studierendenwerk-aachen.de";
        int position = MainActivity.getCurrentMensa();
        switch(position){
            case 0: page="/speiseplaene/ahornstrasse-w.html"; getSupportActionBar().setTitle("Mensa Ahorn"); break;
            case 1: page="/speiseplaene/vita-w.html"; getSupportActionBar().setTitle("Mensa Vita"); break;
            case 2: page="/speiseplaene/academica-w.html"; getSupportActionBar().setTitle("Mensa Academica"); break;
            case 3: page="/speiseplaene/templergraben-w.html"; getSupportActionBar().setTitle("Bistro Templergraben"); break;
            case 4: page="/speiseplaene/bayernallee-w.html"; getSupportActionBar().setTitle("Mensa Bayernallee"); break;
            case 5: page="/speiseplaene/eupenerstrasse-w.html"; getSupportActionBar().setTitle("Mensa Eupenerstrasse"); break;
            case 6: page="/speiseplaene/goethestrasse-w.html"; getSupportActionBar().setTitle("Mensa Goethe"); break;
            default:page="/speiseplaene/ahornstrasse-w.html"; getSupportActionBar().setTitle("Mensa Ahorn"); break;
        }

        // Language handling
        switch(MainActivity.getCurrentLanguage()){
            case ENG: page=page.replace(".html","-en.html"); break;
            default: break;
        }

        try {
            result = http.execute(host, page).get();
            // Error handling
            if(result.equals("")){
                switch(MainActivity.getCurrentLanguage()){
                    case ENG: showErrorDialog("No data available. Try again?"); break;
                    default: showErrorDialog("Keine Daten vorhanden. Erneut versuchen?"); break;
                }
                return;
            }
            result = parser.formatText(result);
            plan  = parser.parseXML(result);
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }

        ExpandableListView expListView = (ExpandableListView) findViewById(R.id.expandableListView);

        List<String> listDataHeader = new ArrayList<String>();
        HashMap <String, List<String>> listDataChild = new HashMap <String, List<String>>();

        for(int i=0; i<plan.size() ;i++) {
            MenuDay day = plan.get(i);
            listDataHeader.add(day.toString());
            List<String> items = new ArrayList<String>();
            if (day.getSpecial()==1){
                items.add(day.getComment());
            }else{
                for (de.rwth_aachen.mensaapp.MenuItem item : day.getMenu()) {
                    items.add(item.toString());
                }
                switch(MainActivity.getCurrentLanguage()){
                    case ENG: items.add("Extras \n" + day.getExtra()); items.add("Salad \n" + day.getSalad()); break;
                    default: items.add("Beilagen \n" + day.getExtra()); items.add("Salat \n" + day.getSalad()); break;
                }
                // Add comment as item for additional information
                items.add(day.getComment());
            }
            listDataChild.put(day.toString(), items);
        }

        ExpandableListAdapter listAdapter = new ExpandableListAdapter(this, listDataHeader, listDataChild);
        expListView.setAdapter(listAdapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }

        // Set new language
        if(id == R.id.action_eng) {
            MainActivity.setCurrentLanguage(Language.ENG);
        }else if(id == R.id.action_ger) {
            MainActivity.setCurrentLanguage(Language.GER);
        }
        // Reload data to update language
        loadMenu();

        return super.onOptionsItemSelected(item);
    }

    public void showErrorDialog(final String message){
        new AlertDialog.Builder(this)
                .setMessage(message)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        loadMenu();
                    }
                })
                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // do nothing
                    }
                })
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
        return;
    }

    public static Language getCurrentLanguage() {
        return currentLanguage;
    }

    public static void setCurrentLanguage(Language currentLanguage) {
        MainActivity.currentLanguage = currentLanguage;
    }

    public static int getCurrentMensa() {
        return currentMensa;
    }

    public static void setCurrentMensa(int currentMensa) {
        MainActivity.currentMensa = currentMensa;
    }

    public String getCurrentTitle() {
        return currentTitle;
    }

    public void setCurrentTitle(String currentTitle) {
        this.currentTitle = currentTitle;
    }
}
